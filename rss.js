#!/bin/node
const http = require('http')
const https = require('https')

const PORT = Number(process.env['PORT']) || 55011

function httpsRequest(options, body) {
    return new Promise(
        function(resolve, reject) {
            if (body) {
                let headers = options.headers || {}
                headers['Content-Length'] = body.length
                options.headers = headers
            }
            const req = https.request(options, res => {
                if (res.statusCode != 200)
                    reject(res)
        
                let data = Buffer.alloc(0)
                res.on('data', d => data += d)
                res.on('end', () => resolve(data.toString('utf-8')))
            })
        
            req.on('error', error => reject(error))

            if (body)
                req.write(body)
            req.end()
        })
}

function dliveGraphQLRequest(data) {
    const options = {
        hostname: 'graphigo.prd.dlive.tv',
        port: 443,
        path: '/',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return httpsRequest(options, data)
}

function dliveRSSRender(channel, livestream, replays) {
    return `\
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
    <channel>
        <title>dlive.tv @${channel} Channel Feed</title>
        <link>https://dlive.tv/${channel}</link>
        <description>Follow your dlive channels using your favorite RSS reader!</description>
        <lastBuildDate>${formatDateTime(Date.now())}</lastBuildDate>
        <docs>https://validator.w3.org/feed/docs/rss2.html</docs>${livestream == null ? '' : `
        <item>
            <title><![CDATA[[LIVE] ${livestream.title}]]></title>
            <description><![CDATA[Watch live at <a href="https://dlive.tv/${channel}">https://dlive.tv/${channel}</a>.]]></description>
            <link>https://dlive.tv/${channel}</link>
            <guid>${livestream.permlink}</guid>
            <pubDate>${formatDateTime(Number(livestream.createdAt))}</pubDate>
        </item>`}${replays == null ? '' : replays.map((replay) => `
        <item>
            <title><![CDATA[${replay.title}]]></title>
            <description><![CDATA[Watch the replay at <a href="https://dlive.tv/p/${replay.permlink}">https://dlive.tv/p/${replay.permlink}</a>.
                <br/>
                <br/>
                <img src="${replay.thumbnailUrl}" width="400" />]]></description>
            <link>https://dlive.tv/p/${replay.permlink}</link>
            <guid isPermaLink="true">https://dlive.tv/p/${replay.permlink}</guid>
            <pubDate>${formatDateTime(Number(replay.createdAt))}</pubDate>
        </item>`).join('')}
    </channel>
</rss>`
}

function dliveRSSGenerate(channel) {
    return new Promise(
        function(resolve, reject) {
            Promise
            .all([dliveGraphQLRequest(`{"operationName":"LivestreamPageRefetch","variables":{"displayname":"${channel}","add":false,"isLoggedIn":false},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"9d63f0589cdb2bf260d53a5783b8f24b7ad151071c5c47ff31a76ce7f521745c"}}}`),
                  dliveGraphQLRequest(`{"operationName":"LivestreamProfileReplay","variables":{"displayname":"${channel}","first":20},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"913ee036eb38d230cdc0d46c8d3a0b016227b3ee2bf3f6b2f501a0a238d43e9a"}}}`)])
            .then(
                function(values) {
                    const JSONValues = values.map((v) => JSON.parse(v))
                    if (JSONValues.every((v) => v.data.userByDisplayName == null)) {
                        reject()
                        return
                    }
                    const livestream = JSONValues[0].data.userByDisplayName.livestream
                    const replays = JSONValues[1].data.userByDisplayName.pastBroadcasts.list
                    resolve(dliveRSSRender(channel, livestream, replays))
                },
                (reason) => reject(reason))
        })
}

function rokfinRestRequest(query) {
    const options = {
        hostname: 'prod-api-v2.production.rokfin.com',
        port: 443,
        path: `${query}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0',
        },
    }
    return httpsRequest(options)
}

function rokfinRSSRender(channel, posts) {
    return `\
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
    <channel>
        <title>rokfin.com @${channel} Channel Feed</title>
        <link>https://rokfin.com/${channel}</link>
        <description>Follow your rokfin channels using your favorite RSS reader!</description>
        <lastBuildDate>${formatDateTime(Date.now())}</lastBuildDate>
        <docs>https://validator.w3.org/feed/docs/rss2.html</docs>${posts == null ? '' : posts.map((post) => `
        <item>
            <title><![CDATA[${post.content.contentTitle}]]></title>
            <description><![CDATA[
                Play online at <a href="https://www.hlsplayer.net/#type=m3u8&src=${encodeURIComponent(`${post.content.contentUrl}?start=0`)}">
                    https://www.hlsplayer.net/#type=m3u8&src=${encodeURIComponent(`${post.content.contentUrl}?start=0`)}
                </a>.<br/><br/>
                ${post.content.contentDescription}${post.content.thumbnailUrl1 == null ? '' : `<br/><br/>
                <img src="${post.content.thumbnailUrl1}" width="400" />`}]]>
            </description>
            <link>https://rokfin.com/post/${post.id}/${post.content.contentTitle.replace(/[^a-zA-Z0-9 ]/g, '').replace(/ /g, '-')}</link>
            <enclosure url="${post.content.contentUrl}?start=0" type="application/x-mpegUrl"/>
            <guid isPermaLink="true">https://rokfin.com/post/${post.id}/</guid>
            <pubDate>${formatDateTime(post.creationDateTime)}</pubDate>
        </item>`).join('')}
    </channel>
</rss>`
}

function rokfinRSSGenerate(channel) {
    return new Promise(
        function(resolve, reject) {
            rokfinRestRequest(`/api/v2/public/user/${channel}/posts?page=0&size=12`).then(
                function(response) {
                    const jsonResponse = JSON.parse(response)
                    const posts = jsonResponse.content
                    if (posts == null) {
                        reject()
                        return
                    }
                    resolve(rokfinRSSRender(channel, posts))
                },
                (reason) => reject(reason)
            )
        }
    )
}

function httpreqHandler(req, res) {
    let onSuccess = (rss) => {
        const buf = Buffer.from(rss, 'utf-8')
        res.writeHead(200, {
            'Content-Type': 'application/rss+xml; charset=utf-8',
            'Content-Length': buf.length,
        })
        res.write(buf)
        res.end()
    }
    let onFailure = () => {
        res.writeHead(404)
        res.end()
    }
    const match = /^\/*([^/]+)\/(.+)/.exec(req.url)
    if (match == null) {
        onFailure()
        return
    }
    const website = match[1]
    const channel = match[2]
    switch (website.toLocaleLowerCase()) {
        case "dlive.tv":
            dliveRSSGenerate(channel).then(rss => onSuccess(rss), _ => onFailure())
            break
        case "rokfin.com":
            rokfinRSSGenerate(channel).then(rss => onSuccess(rss), _ => onFailure())
            break
        default:
            onFailure()
    }
}

const server = http.createServer(httpreqHandler)
server.listen(PORT)

function formatDateTime(datetime) {
    return new Date(datetime).toUTCString()
}
