Dlive.tv and Rokfin.com RSS feed generator
==========================================


[![Deploy on Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=bitbucket.org/pdev101/rssgen)

## Getting started

- For dlive.tv use the url `https://online-video-platforms-rss.herokuapp.com/dlive.tv/channelname`. Replace `channelname` with the name of your channel.

- For rokfin.com use the url `https://online-video-platforms-rss.herokuapp.com/rokfin.com/channelname`. Replace `channelname` with the name of your channel.

