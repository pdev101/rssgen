FROM alpine:edge
RUN apk add --no-cache nodejs
ADD rss.js /
CMD node /rss.js
